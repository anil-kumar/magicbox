<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventImage extends Model
{
    protected $table = 'event_images';
    protected $fillable = [

        'image', 'event_guest_id', 'event_id', 'moderation',

    ];
}
