@extends('layout.app')
@section('title')
	Event-images
@endsection
@section('content')
    <div class="container mt-5"> 	
    	@for($x=0;$x<$rows;$x++)
            <div class="row">
            	@for($k=0;$k<6;$k++)
            		@if(($x*6 + $k)<$total)
		                <div class="col-sm-2 p-2">
		                    <a href="{{asset($arrImages[$x*6 + $k])}}" class="image-popup-no-margins">
		                    	<img class="w-100 " style="height:100px;" src="{{asset($arrImages[$x*6 + $k])}}">
		                	</a>
		                </div>
		            @endif
                @endfor                
            </div>
        @endfor        
    </div>
    <!-- <div>
        <div class="d-flex flex-wrap justify-content-center align-items-center h-100">
            <div class="login-section py-5 px-md-4 bg-white">
                <a href="#" class="logo-login">
                    <img src="img/logo-login.png" alt="logo">
                </a>
                <form action="#">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group mb-1">
                        <label>Email ID</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <span class="d-block mb-3">E-mail is used to send pictures after the event</span>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Accept Terms & Condition</label>
                    </div>
                    <div class="form-group">
                        <button class="gradient-pink btn text-white d-block w-100 border-0" type="submit">Next</button>
                    </div>
                </form>
            </div>
        </div>
    </div> -->

@endsection
@section('scripts')
<script>
$(document).ready(function() {

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

});
</script>
@endsection