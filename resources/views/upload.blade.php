@extends('layout.app')
@section('title')
	upload image
@endsection
@section('content')
	<div class="backgrund-bg bg-dark-black ">
        <div class="d-flex flex-wrap justify-content-center align-items-center h-100">
            <div class="uploads">
                <div class="d-flex flex-wrap justify-content-center align-items-center h-100">
                    <div>
                        <h2>Upload</h2>
                        <button class="btn uploading-btn gradient-pink text-white" data-toggle="modal"
                            data-target="#pictureChangeModal">Upload Picture</button>

                        <button class="btn uploading-btn btn-outline" data-toggle="modal"
                            data-target="#textChangeModal">Upload Text</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal picture -->
    <div class="modal fade bg-dark-black" id="pictureChangeModal" tabindex="-1" role="dialog"
        aria-labelledby="pictureChangeModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered custom-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Upload Picture</h2>
                    <form action="{{route('guest.storeImage')}}" class="dropzone needsclick dz-clickable" id="upload" method="POST" enctype="multipart/form-data">
                    	@csrf
                    	<input type="hidden" value="{{$guest->id}}" name="guest_id">
                    	<input type="hidden" value="{{$guest->event_id}}" name="event_id">
                    	<div class="dz-message needsclick">
                            <img src="{{asset('images/upload.png')}}" alt="">
                            <button type="button" class="dz-button">Drop files here or click to upload.</button><br><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal picture -->
    <div class="modal fade bg-dark-black" id="textChangeModal" tabindex="-1" role="dialog"
        aria-labelledby="textChangeModelTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered custom-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Upload Text</h2>
                    <div class="text-body">
                        <form action="{{ route('guest.storeText') }}" method="POST">
                        	@csrf
                        	<input type="hidden" value="{{$guest->id}}" name="guest_id">
                    		<input type="hidden" value="{{$guest->event_id}}" name="event_id">
                            <div class="textArea">
                                <textarea name="text" class="form-control" placeholder="Type here text"></textarea>
                                <span class="emoji-icon">
                                    <img src="{{asset('images/emoji.png')}}" alt="emoji">
                                </span>
                            </div>
                            <input type="hidden" value="white" name="color" id="color">
                            <div class="d-flex flex-wrap justify-content-between mb-3">
                                <span class="bg-box color-text">Color:</span>
                                <span data-value = 'white' class="bg-white bg-box active"></span>
                                <span data-value = 'pink' class="bg-pink bg-box"></span>
                                <span data-value = 'sky-blue' class="bg-sky-blue bg-box"></span>
                                <span data-value = 'green' class="bg-green bg-box"></span>
                                <span data-value = 'teal' class="bg-teal bg-box"></span>
                                <span data-value = 'blue' class="bg-blue bg-box"></span>
                                <span data-value = 'indigo' class="bg-indigo bg-box"></span>
                                <span data-value = 'purple' class="bg-purple bg-box"></span>
                                <span data-value = 'red' class="bg-red bg-box"></span>
                                <span data-value = 'orange' class="bg-orange bg-box"></span>
                            </div>
                            <button type="submit" class="btn gradient-pink text-white mt-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .text-body .flex-wrap span.active{
            border: 3px solid lightslategray;
        }
    </style>
@endsection
@section('scripts')
    <script>
        $('.bg-box').on('click', function(){
            document.getElementById('color').value = $(this).attr('data-value');
            $('.bg-box').removeClass('active');
            $(this).addClass('active');
        });

        Dropzone.options.upload = {
            acceptedFiles : 'image/*',
            queuecomplete: function() {
                alert('Uploading completed');
                this.removeAllFiles();
            }
        };
    </script>
@endsection
