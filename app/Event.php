<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Event extends Authenticatable
{
    use Notifiable;

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'order_code', 'bar_code', 'event_date', 'moderation'

    ];
    public function eventGuest(){
    	return $this->hasMany('App\EventGuest');
    }
}
