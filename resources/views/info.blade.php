@extends('layout.app')
@section('title')
	Message
@endsection
@section('content')
    <div class="backgrund-bg" style="background-image: url('{{asset('images/login-bg.jpg')}}');">
        <div class="d-flex flex-wrap justify-content-center align-items-center h-100">
            <div class="login-section py-5 px-md-4 bg-white">
                <a href="#" class="logo-login">
                    <img src="{{asset('images/logo-login.png')}}" alt="logo">
                </a>
                <h3 class="text-center"><p style="color: grey">{{$message}}</p></h3>
            </div>
        </div>
    </div>
@endsection