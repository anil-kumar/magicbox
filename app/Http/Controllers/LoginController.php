<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventSetting;
use App\Event;
use App\EventImage;
use App\EventText;
use App\EventGuest;
use PrestaShopWebservice;
use Auth;
use QrCode;
use DB;
use Carbon\Carbon;

class LoginController extends Controller
{
	protected function generateBarCode($orderCode){
    	$image = $orderCode.'.png';
    	QrCode::format('png')->size(200)->errorCorrection('H')->
    		generate("https://magictv.appexperts.us/guest-login/?code=$orderCode", public_path("images/$image"));
    	return $image;
    }

    public function index(Request $request){
	    if($request->code){
	    	$event = Event::where('order_code', $request->code)->first();
	    	if($event == null){

                $url = 'https://www.magictv.fun/api/';
                $key = '?ws_key=BLPT6LHC7V8ZCX1NEZ33FVFMKRWVBWYL';
                $format = '&output_format=JSON';
                $reference = $request->code;
                $curl_reference = curl_init();
                curl_setopt($curl_reference, CURLOPT_URL, $url.'orders'.$key.'&filter[reference]=['.$reference.']'.$format);
                curl_setopt($curl_reference, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl_reference);
                curl_close($curl_reference);
                $data = json_decode($result, true);
                if(count($data)==0){
                    return abort(404);
                }

                $id = $data['orders'][0]['id'];
                $detail = DB::connection('magic')->table('ps_custom_field_userdata')->where('id_order', $id)->first();
                
                $date = date('Y-m-d');
                $event_date = Carbon::createFromFormat('Y-m-d', $detail->field_value);
                $current_date = Carbon::createFromFormat('Y-m-d', $date);
                $diff = $event_date->diffInDays($current_date, false);  

                $curl_order = curl_init();
                curl_setopt($curl_order, CURLOPT_URL, $url.'orders/'.$id.$key.$format);
                curl_setopt($curl_order, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl_order);
                curl_close($curl_order);
                $order_details = json_decode($result, true);
                $order_state = $order_details['order']['current_state'];

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url.'order_states/'.$order_state.$key.$format);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl);
                curl_close($curl);
                $data = json_decode($result, true);
                $paid = $data['order_state']['paid'];

                // if($diff < -3 || $diff > 2){
                //     if($diff < -3){
                //         $message = 'You can not initiate this event right now. You can only login from '.$date1.' to '.$date2.'.';
                //         return view('info', compact('message'));
                //     }
                //     else{
                //         $message = 'Time to initiate this event is over.';
                //         return view('info', compact('message'));
                //     }
                // }

                if($paid == 0){
                    $message = 'You can not initiate this event due to incomplete payment.';
                    return view('info', compact('message'));
                }
                else{
                    $image = $this->generateBarCode($request->code);
                    $image = 'images/'.$image; 
                    $event = Event::create([
                        'order_code' => $request->code,
                        'event_date' => $detail->field_value,
                        'bar_code'   => $image,
                    ]);
                    Auth::guard('event')->login($event, true);        
                    if (Auth::guard('event')->check()) {
                        // Authentication passed...
                        return redirect(route('setting'));
                    }
                    else{
                        return 'something went wrong';
                    }  
                }	 
	    	}
            $date = date('Y-m-d');
            $event_date = Carbon::createFromFormat('Y-m-d', $event->event_date);
            $current_date = Carbon::createFromFormat('Y-m-d', $date);
            $diff = $event_date->diffInDays($current_date, false);
            
            // if($diff < -3 || $diff > 2){
            //     if($diff < -3){
            //         $date1 = $date2 = $event_date;
            //         $date1 = $date1->subDays(3)->format('Y-m-d');
            //         $date2 = $date2->addDays(5)->format('Y-m-d');
            //         $message = 'You can not login to the event right now. You can only login from '.$date1.' to '.$date2.'.';
            //         return view('info', compact('message'));
            //     }
            //     else{
            //         $message = 'Event is over, now you can not login to this event.';
            //         return view('info', compact('message'));
            //     }
            // }
	    	Auth::guard('event')->login($event, true);        
    		if (Auth::guard('event')->check()) {
            	// Authentication passed...
            	return redirect(route('user.admin'));
        	}
        	else{
        		return 'something went wrong';
        	}    		    	
	    }
    	else{
            echo phpinfo();
            die;
    		return abort(404);
    	}
    }

    public function setting(Request $request){
    	$setting = EventSetting::where('event_id', Auth::guard('event')->user()->id)->first();
    	
    	return view('index', compact('setting'));
    	
    }

    public function store(Request $request){
    	$setting = EventSetting::where('event_id', Auth::guard('event')->user()->id)->first();
    	if($setting == null){
	    	if($request->moderation){
	    		$event_setting = EventSetting::create([
	    			'title' => $request->title,
	    			'event_id' => Auth::guard('event')->user()->id,
	    			'delay' => $request->delay,
	    			'is_moderation' => 1,
	    			
	    		]);
	    	}
	    	else{
	    		$event_setting = EventSetting::create([
	    			'title' => $request->title,
	    			'delay' => $request->delay,
	    			'event_id' => Auth::guard('event')->user()->id,
                    'is_moderation' => 0,
	    			
	    		]);
	    	}
            // $event_images = EventImage::where('event_id', $event_setting->event_id)->get();
            // if($event_setting->is_moderation = 0){

            // }
	    }
	    else{
	    	$setting->title = $request->title;
	    	$setting->delay = $request->delay;
	    	if($request->moderation){
	    		$setting->is_moderation = 1;
	    	}
	    	else{
	    		$setting->is_moderation = 0;
	    	}
	    	$setting->save();
            // $event_images = EventImage::where('event_id', $setting->event_id)->get();
            // if($setting->is_moderation = 0 && count($event_images)>0){
            
            //     foreach($event_images as $image){
            //         $image->moderation = 0;
            //         $image->save();
            //     }
            // }
            // if($setting->is_moderation = 1 && count($event_images)>0){
            //     foreach($event_images as $image){
            //         $image->moderation = 1;
            //         $image->save();
            //     }
            // }

	    }
    	return redirect(route('user.admin'));
    }

    public function admin(){
    	$moderation = EventSetting::where('event_id', Auth::guard('event')->user()->id)->first();
    	$images = EventImage::where('event_id', Auth::guard('event')->user()->id)->get();
    	$guests = EventGuest::where('event_id', Auth::guard('event')->user()->id)->get();
        $texts = EventText::where('event_id', Auth::guard('event')->user()->id)->get();
    	return view('admin', compact('images', 'guests', 'moderation', 'texts'));
    }

    public function slider(){
    	$user = Auth::guard('event')->user();
        $event = Event::find($user->id);
        $event_setting = EventSetting::where('event_id', $event->id)->first();
        $event->moderation = 0;
        $event->save();
    	$event_setting = EventSetting::where('event_id', Auth::guard('event')->user()->id)->first();
    	
	    $images = EventImage::where('event_id', $user->id)->where('moderation', 0)
            ->get();
        $texts = EventText::where('event_id', $user->id)->where('moderation', 0)
            ->get();
	    
        $data = [];
        $i = 0;
        $j = 0;
        ;
        while($i < count($images) && $j < count($texts)){
            if($images[$i]->created_at <= $texts[$j]->created_at){
                array_push($data, [
                    'image' => $images[$i]->image,
                    'created_at' => $images[$i]->created_at,
                    'id' => 'image'.$images[$i]->id
                ]);
                $i++;
            }
            else{
                array_push($data, [
                    'image' => $texts[$j]->text,
                    'created_at' => $texts[$j]->created_at,
                    'color_code' => $texts[$j]->color_code,
                    'id' => 'text'.$texts[$j]->id
                ]);
                $j++;
            }
        }

        while($i < count($images)){
            array_push($data, [
                    'image' => $images[$i]->image,
                    'created_at' => $images[$i]->created_at,
                    'id' => 'image'.$images[$i]->id
                ]);
            $i++;
        }

        while($j < count($texts)){
            array_push($data, [
                    'image' => $texts[$j]->text,
                    'created_at' => $texts[$j]->created_at,
                    'color_code' => $texts[$j]->color_code,
                    'id' => 'text'.$texts[$j]->id
                ]);
            $j++;
        }
        
    	return view('slider', compact('user', 'images', 'data', 'texts', 'event_setting'));
    }

    public function playPauseSlider(Request $request){
        $event = Event::find(Auth::guard('event')->user()->id);
        
        return response()->json([
            'data' => $event,
        ]);
    }

    public function moderation(Request $request){
    	$event_image = EventImage::find($request->value);
    	if($request->moderation == "on"){
    		$event_image->moderation = 0;
    	}
    	else{
    		$event_image->moderation = 1;
    	}
    	$event_image->save();   	
    }

    public function textmoderation(Request $request){
        $event_text = EventText::find($request->value);
        if($request->moderation == "on"){
            $event_text->moderation = 0;
        }
        else{
            $event_text->moderation = 1;
        }
        $event_text->save();       
    }

    public function playSlider(Request $request){
        $event = Event::find(Auth::guard('event')->user()->id);
        $event->moderation = 0;
        $event->save();
    }

    public function pauseSlider(Request $request){
        $event = Event::find(Auth::guard('event')->user()->id);
        $event->moderation = 1;
        $event->save();
    }

    public function slideRefresh(Request $request){
    
    	// $images = EventImage::where('event_id', Auth::guard('event')->user()->id)->get();
    	// foreach($images as $image){
    	// 	$created = $image->created_at;
    	// }
    	// if($created > $request->created){
    	

    	$addimages = EventImage::where('event_id', Auth::guard('event')->user()->id)
    	->where('moderation', 0);


        if($request->created) {
    	   $addimages = $addimages->where('created_at','>',$request->created);
        }

        $addimages = $addimages->get();

        $addtexts = EventText::where('event_id', Auth::guard('event')->user()->id)
            ->where('moderation', 0);
        if($request->createdtext) {
            $addtexts = $addtexts->where('created_at','>',$request->createdtext);
            
        }
        $addtexts = $addtexts->get();

        $adddata = [];
        $i = 0;
        $j = 0;
        
        while($i < count($addimages) && $j < count($addtexts)){
            if($addimages[$i]->created_at <= $addtexts[$j]->created_at){
                array_push($adddata, $addimages[$i]->image);
                $i++;
            }
            else{
                array_push($adddata, $addtexts[$j]->text);
                $j++;
            }
        }

        while($i < count($addimages)){
            array_push($adddata, $addimages[$i]->image);
            $i++;
        }

        while($j < count($addtexts)){
            array_push($adddata, $addtexts[$j]->text);
            $j++;
        }
    	
    	$newImages = [];
    	foreach($addimages as $key => $image){
   			$newImages[$key]['image']=$image->image;
   			$newImages[$key]['date']=$image->created_at;
            $newImages[$key]['id']='image'.$image->id;
    		//array_push($newImages,$image->image);
    	}

        $newText = [];
        foreach($addtexts as $key => $text){
            $newText[$key]['text']=$text->text;
            $newText[$key]['date']=$text->created_at;
            $newText[$key]['color_code']=$text->color_code;
            $newText[$key]['id']='text'.$text->id;
            //array_push($newImages,$image->image);
        }

    	return response()->json([
            'status' => 'success',
            'data' => $newImages,
            'addtexts' => $newText,
        ]);
    }

    public function disableImage(Request $request){
        $disabledimages = EventImage::where('event_id', Auth::guard('event')->user()->id)->where('moderation', 1)->get();
        $disableimages = [];
        foreach($disabledimages as $key => $disableimage){
            $disableimages[$key]['image']=$disableimage->image;
            $disableimages[$key]['date']=$disableimage->created_at;
            $disableimages[$key]['id']='image'.$disableimage->id;
        }
        return response()->json([
            'status' => 'success',
            'disableimagedata' => $disableimages,
            
        ]);
    }

    public function disableText(Request $request){
        $disabledtexts = EventText::where('event_id', Auth::guard('event')->user()->id)->where('moderation', 1)->get();
        $disabletexts = [];
        foreach($disabledtexts as $key => $disabletext){
            $disabletexts[$key]['text']=$disabletext->text;
            $disabletexts[$key]['date']=$disabletext->created_at;
            $disabletexts[$key]['color_code']=$disabletext->color_code;
            $disabletexts[$key]['id']='text'.$disabletext->id;
            //array_push($newImages,$image->image);
        }
        return response()->json([
            'status' => 'success',
            'disabletextdata' => $disabletexts,
            
        ]);
    }

}
