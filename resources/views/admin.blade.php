@extends('layout.app')
@section('title')
	admin
@endsection
@section('content')

    <header class="custom-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-lg p-0  navbar-dark  ">
                <a class="navbar-brand" href="#">
                    <img src="{{asset('images/logo-white.png')}}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item  text-center">
                            <a class="nav-link" href="{{route('user.slider')}}">
                                <div class="menu-icon"><img src="{{asset('images/slideshow.png')}}"></div>
                                Slideshow
                            </a>
                        </li>
                        <li class="nav-item text-center active">
                            <a class="nav-link" href="{{route('user.admin')}}">
                                <div class="menu-icon"><img src="{{asset('images/admin.png')}}"></div>
                                Admin
                            </a>
                        </li>
                        <li class="nav-item text-center">
                            <a class="nav-link" href="#">
                                <div class="menu-icon"><img src="{{asset('images/documentation.png')}}"></div>
                                Documentation
                            </a>
                        </li>
                        <li class="nav-item text-center">
                            <a class="nav-link" href="{{route('setting')}}">
                                <div class="menu-icon"><img src="{{asset('images/settings.png')}}"></div>
                                Settings
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <div class="container mt-5">
        <div class="dark-bg p-2 p-sm-5 justify-content-between">
            <div class="row justify-content-between">
                <div>
                    <h3 class="border-bottom-custom">Moderation activation</h3>
                </div>
                <div class="mr-4">
                    <a href="#" class="mbtn {{(!Auth::guard('event')->user()->moderation)?'d-none':null}}" onclick="playSlide();">
                        Play Slide 
                        <svg class="svg-icon" width="40px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                            <g id="icon-shape">
                                <path d="M2.92893219,17.0710678 C6.83417511,20.9763107 13.1658249,20.9763107 17.0710678,17.0710678 C20.9763107,13.1658249 20.9763107,6.83417511 17.0710678,2.92893219 C13.1658249,-0.976310729 6.83417511,-0.976310729 2.92893219,2.92893219 C-0.976310729,6.83417511 -0.976310729,13.1658249 2.92893219,17.0710678 L2.92893219,17.0710678 Z M15.6568542,15.6568542 C18.7810486,12.5326599 18.7810486,7.46734008 15.6568542,4.34314575 C12.5326599,1.21895142 7.46734008,1.21895142 4.34314575,4.34314575 C1.21895142,7.46734008 1.21895142,12.5326599 4.34314575,15.6568542 C7.46734008,18.7810486 12.5326599,18.7810486 15.6568542,15.6568542 Z M7,6 L15,10 L7,14 L7,6 Z" id="Combined-Shape"></path>
                            </g>
                        </g>
                        </svg>
                    </a>
                    <a href="#" class="mbtn {{(Auth::guard('event')->user()->moderation)?'d-none':null}}" onclick="pauseSlide();">
                        Pause Slide 
                        <svg class="svg-icon" width="40px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                            <g id="icon-shape">
                                <path d="M2.92893219,17.0710678 C6.83417511,20.9763107 13.1658249,20.9763107 17.0710678,17.0710678 C20.9763107,13.1658249 20.9763107,6.83417511 17.0710678,2.92893219 C13.1658249,-0.976310729 6.83417511,-0.976310729 2.92893219,2.92893219 C-0.976310729,6.83417511 -0.976310729,13.1658249 2.92893219,17.0710678 L2.92893219,17.0710678 Z M15.6568542,15.6568542 C18.7810486,12.5326599 18.7810486,7.46734008 15.6568542,4.34314575 C12.5326599,1.21895142 7.46734008,1.21895142 4.34314575,4.34314575 C1.21895142,7.46734008 1.21895142,12.5326599 4.34314575,15.6568542 C7.46734008,18.7810486 12.5326599,18.7810486 15.6568542,15.6568542 Z M7,6 L9,6 L9,14 L7,14 L7,6 Z M11,6 L13,6 L13,14 L11,14 L11,6 Z" id="Combined-Shape"></path>
                            </g>
                        </g>
                        </svg>
                    </a>
                </div>
            </div>
            <ul class="nav nav-pills mb-3" id="pills-Tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Image</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="tab" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Text</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
            <div class="table-responsive tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <table class="table moderation-table ">
                    <thead>
                        <tr>
                            <th scope="col">Image</th>
                            <th scope="col">User Name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($guests as $guest)
                    		@foreach($images as $image)
                    		@if($guest->id == $image->event_guest_id)
                        <tr>
                            <td scope="row">
                                <div class="user-image">

                                <a href="{{asset($image->image)}}" class="image-popup-no-margins">
                                    
                                    <img src="{{asset($image->image)}}">
                                </a>

                                </div>

                            </td>
                            <td>{{$guest->name}}</td>
                            <td>{{$image->created_at->format('d/m/Y')}}</td>
                            <td>{{$image->created_at->format('H:i A')}}</td>
                            <td><label class="switch">
                                    @if($moderation->is_moderation == 0)
                                        @if($image->moderation == 0)
                                        <input type="checkbox" checked disabled>
                                        <span class="slider round"></span>
                                        @else
                                        <input type="checkbox" disabled>
                                        <span class="slider round"></span>
                                        @endif
                                    @else
                                    <form action="{{route('user.moderation')}}" method="POST" class="formbutton">
                                    @csrf
                                    <input type="hidden" name="value" value="{{$image->id}}">
                                        @if($image->moderation == 0)
                                        <input type="checkbox" name="moderation" class="checkbutton" checked>
                                        @else
                                        <input type="checkbox" name="moderation" class="checkbutton">
                                        @endif
                                    <span class="slider round"></span>
                                    </form>
                                    @endif
                                    
                                    
                                </label></td>
                        </tr>
                        	@endif
                        @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="table-responsive tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <table class="table moderation-table ">
                    <thead>
                        <tr>
                            <!-- <th scope="col">User</th> -->
                            <th scope="col">User Name</th>
                            <th scope="col">Slide Text</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($guests as $guest)
                            @foreach($texts as $text)
                            @if($guest->id == $image->event_guest_id)
                        <tr>
                            <td>{{$guest->name}}</td>
                            <td>{{$text->text}}</td>
                            <td>{{$text->created_at->format('d/m/Y')}}</td>
                            <td>{{$text->created_at->format('H:i A')}}</td>
                            <td><label class="switch">
                                    @if($moderation->is_moderation == 0)
                                    <input type="checkbox" checked disabled>
                                    <span class="slider round"></span>
                                    @else
                                    <form action="{{route('user.text.moderation')}}" method="POST" class="formbutton">
                                    @csrf
                                        <input type="hidden" name="value" value="{{$text->id}}">
                                        @if($text->moderation == 0)
                                        <input type="checkbox" name="moderation" class="checkbutton" checked>
                                        @else
                                        <input type="checkbox" name="moderation" class="checkbutton">
                                        @endif
                                    <span class="slider round"></span>
                                    </form>
                                    @endif
                                </label>
                            </td>
                        </tr>
                            @endif
                        @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>

    <form action="{{route('play.slide')}}" method="POST" id="playSlide">
        @csrf
    </form>

    <form action="{{route('pause.slide')}}" method="POST" id="pauseSlide">
        @csrf
    </form>

    <footer class="footer mt-auto py-3">
        <div class="container text-center">
            <span class="text-muted">© 2020 Magic TV. All rights reserved.</span>
        </div>
    </footer>
@endsection
@section('scripts')
<script>
    console.log(window.location.pathname);
    $(document).on('click', '.checkbutton', onCheckbuttonClicked);

    function onCheckbuttonClicked(event) {
        console.log('string form');
    form =  event.target.closest('form');// document.getElementByClassName('formbutton');
    let eventForm = new FormData(form)
    $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {
            // if (response.status == 'success') {
                
            //     $modalEditAppointment.modal('hide');
            // } else {
            //     swal({
            //         title: "Error",
            //         text: response.data,
            //         icon: "warning",
            //     });
            // }
        },
        error: function() {
            // swal({
            //     title: "Error",
            //     text: "Some Error Occured! Please Try Again",
            //     icon: "warning",
            // });
        },
        dataType: 'json'
    });
}
    function playSlide(){
        $('.mbtn').toggleClass('d-none');
        form = document.getElementById('playSlide');
        let eventForm = new FormData(form)
        $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {
        },
        error: function() {
            
        },
        dataType: 'json'
        });
    }
    function pauseSlide(){
        $('.mbtn').toggleClass('d-none');
        form = document.getElementById('pauseSlide');
        let eventForm = new FormData(form)
        $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {            
        },
        error: function() {
            
        },
        dataType: 'json'
        });
    }
</script>
<script>
    $(document).ready(function() {

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    setInterval(function(){ location.reload() }, 25000);
});
</script>
@endsection