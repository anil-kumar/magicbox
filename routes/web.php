<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','LoginController@index');
Route::post('/event-store', 'LoginController@store')->name('event.store')->middleware('auth:event');
Route::get('/user-admin', 'LoginController@admin')->name('user.admin')->middleware('auth:event');
Route::get('/setting', 'LoginController@setting')->name('setting')->middleware('auth:event');
Route::get('/user-slider', 'LoginController@slider')->name('user.slider')->middleware('auth:event');
Route::post('/refresh-slider', 'LoginController@playPauseSlider')->name('slider.play-pause')->middleware('auth:event');
Route::post('/play-slider', 'LoginController@playSlider')->name('play.slide')->middleware('auth:event');
Route::post('/disable-image', 'LoginController@disableImage')->name('disable.image')->middleware('auth:event');
Route::post('/disable-text', 'LoginController@disableText')->name('disable.text')->middleware('auth:event');
Route::post('/pause-slider', 'LoginController@pauseSlider')->name('pause.slide')->middleware('auth:event');
Route::post('/moderation', 'LoginController@moderation')->name('user.moderation')->middleware('auth:event');
Route::post('/textmoderation', 'LoginController@textmoderation')->name('user.text.moderation')->middleware('auth:event');
Route::post('/refreshSlide', 'LoginController@slideRefresh')->name('slide.refresh')->middleware('auth:event');

Route::get('/guest-login', 'GuestController@index')->name('guest.index');
Route::post('/guest-login/login', 'GuestController@login')->name('guest.login');
ROute::post('/guest-login/store/image', 'GuestController@storeImage')->name('guest.storeImage')->middleware('auth:barcode');
ROute::post('/guest-login/store/text', 'GuestController@storeText')->name('guest.storeText')->middleware('auth:barcode');
Route::get('/all', 'GuestController@sendEmail');
