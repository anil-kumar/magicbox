<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Mail;
use App\Mail\Eventend;
use Illuminate\Console\Command;
use App\Event;
use App\EventGuest;
use App\EventText;
use App\EventImage;
use App\EventSetting;
use Auth;
use Carbon\Carbon;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->info("Healthcheck passed for");
        $date = date('Y-m-d');
        $current_date = Carbon::createFromFormat('Y-m-d', $date);
        $event_date = $current_date->subdays(3)->format('Y-m-d');
        $events = Event::where('event_date', $event_date)->get();
        
        foreach($events as $event){
            $path = 'https://magictv.appexperts.us/all/?code='.$event->id;
            $guests = EventGuest::where('event_id', $event->id)->get();
            $setting = EventSetting::where('event_id', $event_id)->first();
            foreach($guests as $guest){
                $data = [
                    'name' => $guest->name,
                    'message' => 'We are glad that you joined our event '.$setting->title.' on '.$event->event_date.'. Here is the link where you can see all the posts from the event ',
                    'path' => $path
                ];
                Mail::to($guest->email)->queue(new Eventend($data));
            }
        }
        $this->info($event_date);
    }
}
