<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSetting extends Model
{
	protected $table = 'event_settings';
    protected $fillable = [

        'title', 'event_id', 'is_moderation', 'delay',

    ];
}
