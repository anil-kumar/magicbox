<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Welcome to Magic TV</h2>
    </body>
</html>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');
</style>
<table cellpadding="0" cellspacing="0" align="center" bgcolor="#F2F2F2" border="0" width="100%">
    <tbody>
        <tr>
            <td align="center">
                <table cellpadding="0" cellspacing="0" align="center" bgcolor="#881E22" border="0" width="600">
                    <tbody>
                        <tr>
                            <td align="center" height="20" style="font-size:1px;line-height:1px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table cellpadding="0" cellspacing="0" align="left" border="0">
                                    <tbody>
                                        <tr>
                                            <h1 style="color: #fff; margin-left: 30px;">Magic TV</h1>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" height="18" style="font-size:1px;line-height:1px">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF" border="0" width="600">
                    <tbody>
                        <tr>
                            <td align="center" style="padding:0 px">
                                <table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="center" height="25" style="font-size:1px;line-height:1px">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding:0 30px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="25" style="font-size:1px;line-height:1px">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding:0 30px">
                                                <div
                                                    style="color:#5a5b5f;font-family:Montserrat,Helvetica,Arial,sans-serif;font-size:20px;line-height:27px;font-weight:400;text-align:left">
                                                    <p>Welcome to Magic TV</p>
                                                    <p>Hello {{$data['name']}},</p>
                                                    <p>{{$data['message']}}<a href="{{$data['path']}}">{{$data['path']}}</a></p>                                                   
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="30" style="font-size:1px;line-height:1px">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" style="font-size:1px;line-height:1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding:0 30px">
                                                <div
                                                    style="color:#5a5b5f;font-family:Montserrat,Helvetica,Arial,sans-serif;font-size:16px;line-height:27px;font-weight:400;text-align:left">
                                                    Regards from Magic TV<br>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="50" style="font-size:1px;line-height:1px">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>