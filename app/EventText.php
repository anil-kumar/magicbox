<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventText extends Model
{
    protected $table = 'event_texts';
    protected $fillable = [

        'text', 'event_guest_id', 'event_id', 'color_code', 'moderation'

    ];
}
