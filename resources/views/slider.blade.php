@extends('layout.app')
@section('title')
	Slider
@endsection
@section('content')
    <header class="custom-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-lg p-0  navbar-dark  ">
                <a class="navbar-brand" href="#">
                    <img src="{{asset('images/logo-white.png')}}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item  text-center">
                            <a class="nav-link" href="{{route('user.slider')}}">
                                <div class="menu-icon active"><img src="{{asset('images/slideshow.png')}}"></div>
                                Slideshow
                            </a>
                        </li>
                        <li class="nav-item text-center">
                            <a class="nav-link" href="{{route('user.admin')}}">
                                <div class="menu-icon"><img src="{{asset('images/admin.png')}}"></div>
                                Admin
                            </a>
                        </li>
                        <li class="nav-item text-center">
                            <a class="nav-link" href="#">
                                <div class="menu-icon"><img src="{{asset('images/documentation.png')}}"></div>
                                Documentation
                            </a>
                        </li> 
                        <li class="nav-item text-center">
                            <a class="nav-link" href="{{route('setting')}}">
                                <div class="menu-icon"><img src="{{asset('images/settings.png')}}"></div>
                                Settings
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </header>


    <div class="container mt-5">
        
        <div class="row">
            <div class="col-sm-4 text-center d-flex right-block justify-content-center align-items-center">
                Digital marketing<br>
                Event 2020
            </div>
            <div class="col-sm-8">
                
                <div class="carousal-bg" id="element">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" id="image-slider">
                            <div style="z-index: 999;" class="full-screen-logo d-none">
                                <div>
                                    <img src="{{asset('images/logo-white.png')}}">
                                </div>
                                
                            </div>
                            <div style="z-index:999;" class="qr-code d-none">
                                
                                <div class="row d-flex justify-content-center align-items-center">
                                    <div class="col-sm-8 col-8">
                                        <div class="upload-text-bg">Upload event pictures by scanning</div>
                                    </div>
                                    <div class="col-sm-4 col-4 text-center">
                                        <div class="qr-code-block">
                                            <img src="{{asset('images/'.$user->order_code.'.png')}}"  style="max-width:125px;">
                                            <div class="scan-text mt-2 mb-2">Scan QR Code</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="full-screen-icon" style="z-index:999">
                                <!-- <div class="upload-text-bg">{{$event_setting->title}}</div> -->
                                <button
                                    onclick="openFullscreen();">
                                    <img class="qrcode"
                                        src="{{asset('images/full-screen.png')}}">
                                </button>
                            </div>
                           
                        <?php $createdtext = $created = ''; ?>

                            <div class="carousel-item item-height slide-image active">                     
                                <div class="event-title text-white d-flex justify-content-center align-items-center" style="z-index: 998"><h2 class="text-center">{{$event_setting->title}}</h2></div>
                                <img src="{{asset('images/slideshow.jpg')}}" class="d-block w-100 image-setting" alt="...">
                            </div>
                        @if(count($data) != 0)
                            @foreach($data as $item)
                            <div class="carousel-item item-height slide-image">
                                @if(strpos($item['image'], '.jpg') || strpos($item['image'], '.png') || strpos($item['image'], '.jpeg'))
                                    <?php $created = $item['created_at']; ?>
                                    <img id="{{$item['id']}}" src="{{asset($item['image'])}}" class="d-block w-100 image-setting" alt="...">
                                @else
                                    <?php $createdtext = $item['created_at']; ?>
                                <div id="{{$item['id']}}" class="text-center align-items-center justify-content-center d-flex image-setting" >
                                    <h1  class="" style="color:{{$item['color_code']}}">{{ $item['image'] }}</h1>
                                </div>
                                @endif

                            </div>
                                
                                    @foreach($images as $image)

                                        <?php $created = $image->created_at; ?>
                                      
                                    @endforeach        
                                    @foreach($texts as $text)

                                        <?php $createdtext = $text->created_at; ?>

                                    @endforeach
                                    
                            @endforeach                        
                        @endif

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <form action="{{route('slide.refresh')}}" method="POST" id="refreshform">
        @csrf
        <input type="hidden" value="{{$created}}" name="created">
        <input type="hidden" value="{{$createdtext}}" name="createdtext">
    </form>

    <form action="{{route('slider.play-pause')}}" method="POST" id="play-pause-form">
        @csrf
        <input type="hidden" value="{{$user->id}}" name="id">
    </form>  
    <form action="{{route('disable.image')}}" method="POST" id="disable-image">
        @csrf
        <input type="hidden" value="{{$user->id}}" name="id">
    </form>
    <form action="{{route('disable.text')}}" method="POST" id="disable-text">
        @csrf
        <input type="hidden" value="{{$user->id}}" name="id">
    </form>

    <footer class="footer mt-auto py-3">
        <div class="container text-center">
            <span class="text-muted">© 2020 Magic TV. All rights reserved.</span>
        </div>
    </footer>

    <template id="slideItems">
        <div class="carousel-item item-height slide-image">
            <div class="full-screen-icon" style="z-index:999">

                <button onclick="openFullscreen();"><img src="{{asset('images/full-screen.png')}}">
                </button>
            </div>  
            <img src="img/slideshow.jpg" class="d-block w-100 image-setting" alt="..." data-id="slider-image">
        </div>
    </template>

        <template id="slideItemsTexts">
        <div class="carousel-item item-height slide-image">
            <div class="full-screen-icon" style="z-index:999">

                <button onclick="openFullscreen();"><img src="{{asset('images/full-screen.png')}}">
                </button>
            </div>  
            <div class="text-center align-items-center justify-content-center d-flex image-setting">
                <h1 id="slideText" class=""></h1>
            </div>

        </div>
    </template>
@endsection
@section('scripts')
<script type="text/javascript">
function openFullscreen() {
   let elem = document.getElementById('element');
      if (elem.requestFullscreen) {
    elem.requestFullscreen();
    let qrcode = document.getElementByClassName('qrcode');
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}
// $(document).on('change', '.slide-image', onImageChanged);

    $(document).ready(function(){

        myVar = setInterval("showImage()", 10000);
        myVarSlide = setInterval("playPauseSlide()", 3000);
        disabledimage = setInterval("disabledImage()", 3000);
        disabledtext = setInterval("disabledText()", 3000);

    });
    function setTemplate(data) {
        if ('content' in document.createElement('template')) {
            // Instantiate the table with the existing HTML tbody
            // and the row with the template
            var template = document.querySelector('#slideItems');
            // Clone the new row and insert it into the table
            var CrousalContainer = document.querySelector(".carousel-inner");       
            var clone = document.importNode(template.content, true); 
            let imageTag = clone.querySelector('img[data-id="slider-image"]');
            imageTag.setAttribute('src', data['image']);
            imageTag.setAttribute('id', data['id']);       
            CrousalContainer.appendChild(clone);    
        }else {
          // the HTML template element is not supported.
        }
    }

    function setTemplateText(data) {
        if ('content' in document.createElement('template')) {
            // Instantiate the table with the existing HTML tbody
            // and the row with the template
            var template = document.querySelector('#slideItemsTexts');
            // Clone the new row and insert it into the table
            var CrousalContainer = document.querySelector(".carousel-inner");       
            var clone = document.importNode(template.content, true); 
            console.log(clone.querySelector('#slideText'), data)
            let imageTag = clone.querySelector('#slideText');
            imageTag.innerHTML = data['text']; 
            imageTag.style.color = data['color_code'];   
            CrousalContainer.appendChild(clone);    
            $('#slideText').closest('div').setAttribute('id',data['id']);
        }else {
          // the HTML template element is not supported.
        }
    }

    $('#element').on('mouseover', function(){
        $('.play-pause').removeClass('d-none');
        $('.play-pause').addClass('d-flex');
    });
    $('#element').on('mouseout', function(){
        $('.play-pause').removeClass('d-flex');
        $('.play-pause').addClass('d-none');
    });
    $('.pause-pause').on('click', function(){
        $('.pause-pause').addClass('d-none');
        $('.play-play').removeClass('d-none');
    });
    $('.play-play').on('click', function(){
        $('.play-play').addClass('d-none');
        $('.pause-pause').removeClass('d-none');
    });

    function showImage(){
        console.log('Image updated');
        // console.log(JSON.data.image);
        // var $eventForm = $('#refreshform');
        form = document.getElementById('refreshform');
        let eventForm = new FormData(form)
        $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {
            if(response.data) {

                for (const property in response.data) {
                    setTemplate(response.data[property])
                    document.querySelector('input[name="created"]').value=response.data[property]['date'];
                }
                for (const property in response.addtexts) {
                    setTemplateText(response.addtexts[property])
                    document.querySelector('input[name="createdtext"]').value=response.addtexts[property]['date'];
                }
            }


        },
        error: function() {
            
        },
        dataType: 'json'
    });
    }  
    var state=0;
    console.log(state);
    function playPauseSlide(){
        form = document.getElementById('play-pause-form');
        let eventForm = new FormData(form)
        $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {
            if(state == 1 && response.data.moderation == 0){
                $('.carousel').carousel();
                state = 0;
            }
            else if(state == 0 && response.data.moderation == 1){
                $('.carousel').carousel(0);
                $('.carousel').carousel('pause');
                state = 1;
            }
            else{}
        },
        error: function() {
            
        },
        dataType: 'json'
    });
    }

    function disabledImage(){
        form = document.getElementById('disable-image');
        let eventForm = new FormData(form)
        $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {
            if(response.disableimagedata) {
                
                for (const property of response.disableimagedata){
                    if(property.id) {
                        $(`#${property.id}`).closest('div.carousel-item').remove();
                    }
                    console.log(property);
                }
            }

        },
        error: function() {
            
        },
        dataType: 'json'
        });

    }
    function disabledText(){
        form = document.getElementById('disable-text');
        let eventForm = new FormData(form)
        $.ajax({
        type: "POST",
        url: form.getAttribute('action'),
        data: eventForm,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(response) {
            if(response.disabletextdata) {
                for (const property of response.disabletextdata){
                    if(property.id) {
                        $(`#${property.id}`).closest('div.carousel-item').remove();
                    }
                    console.log(property);
                }
            }

        },
        error: function() {
            
        },
        dataType: 'json'
        });

    }
    
</script>

@endsection 