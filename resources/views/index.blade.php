@extends('layout.app')
@section('title')
	welcome
@endsection
@section('content')
    <div class="backgrund-bg">
        <div class="d-flex flex-wrap justify-content-center align-items-center h-100">
            <div class="welcome-text welcome-width text-center welcome-middle-bg p-5">

                <h2>Welcome To <span class="magic-tv-text">Magic TV</span></h2>
                <form action="{{ route('event.store') }}" method="POST">
                @csrf
                <div class=" middle-width m-auto p-5">
                    <div class="form-group">
                        @if($setting != null)
                        <input type="text" value="{{$setting->title}}" class="form-control title-event" name="title" placeholder="Title of the event">
                        @else
                        <input type="text"  class="form-control title-event" name="title" placeholder="Title of the event">
                        @endif
                    </div>
                    <div class="form-group white-box">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="text-left">
                                    Moderation or not ?
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class=" text-right">
                                    <label class="custom-checkbox">
                                        @if($setting != null && $setting->is_moderation == 1)
                                        <input type="checkbox" name="moderation" checked>
                                        @else
                                        <input type="checkbox" name="moderation">
                                        @endif
                                        <span class="checkmark"></span>
                                    </label>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group white-box">
                        <div class="row">
                            <div class="col-sm-8 col-8">
                                <div class="text-left">
                                    Delay in seconds field
                                </div>
                            </div>
                            <div class="col-sm-4 col-4">
                                <div class=" text-right">
                                    <select class="form-control custom-select" name="delay">
                                        @if($setting != null && $setting->delay == "10 second")
                                        <option>5 second</option>
                                        <option selected>10 second</option>
                                        @else
                                        <option>5 second</option>
                                        <option>10 second</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-4 mb-0">
                        <button class="btn w-100 btn-let-start">Let's Start <img src="{{ asset('images/arrow-next.png')}}"> </button>
                    </div>
                </div>
            	</form>

            </div>
        </div>
    </div>
@endsection