@extends('layout.app')
@section('title')
	Guest-login
@endsection
@section('content')
    <div class="backgrund-bg" style="background-image: url('{{asset('images/login-bg.jpg')}}');">
        <div class="d-flex flex-wrap justify-content-center align-items-center h-100">
            <div class="login-section py-5 px-md-4 bg-white">
                <a href="#" class="logo-login">
                    <img src="{{asset('images/logo-login.png')}}" alt="logo">
                </a>
                <form action="{{ route('guest.login') }}" method="POST">
                	@csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <input type="hidden" value="{{$order_code}}" name="code">
                    <div class="form-group mb-1">
                        <label>Email ID</label>
                        <input type="text" name="email" class="form-control">
                    </div>
                    <span class="d-block mb-3">E-mail is used to send pictures after the event</span>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" name="terms" id="exampleCheck1" required>
                        <label class="form-check-label" for="exampleCheck1">Accept Terms & Condition</label>
                    </div>
                    <div class="form-group">
                        <button class="gradient-pink btn text-white d-block w-100 border-0" type="submit">Next</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection