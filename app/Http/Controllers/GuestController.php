<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventGuest;
use App\EventText;
use App\EventImage;
use App\EventSetting;
use Auth;
use Carbon\Carbon;

class GuestController extends Controller
{
    public function index(Request $request){
    	
    	if($request->code){
    		$order_code = $request->code;
            $event = Event::where('order_code', $request->code)->first();
            $date = date('Y-m-d');
            $event_date = Carbon::createFromFormat('Y-m-d', $event->event_date);
            $current_date = Carbon::createFromFormat('Y-m-d', $date);
            $diff = $event_date->diffInDays($current_date, false);
            // if($diff < -3 || $diff > 2){
            //     if($diff < -3){
            //         $date1 = $date2 = $event_date;
            //         $date1 = $date1->subDays(3)->format('Y-m-d');
            //         $date2 = $date2->addDays(5)->format('Y-m-d');
            //         $message = "The event havn't started yet. You can login from $date1 to $date2.";    
            //         return view('info', compact('message'));
            //     }
            //     else{
            //         $message = 'This event is over';    
            //         return view('info', compact('message'));
            //     }
            // }
    		return view('guest-login', compact('order_code'));
    	}
    	else{
    		return abort(404);
    	}
    }

    public function login(Request $request){
    	$event = Event::where('order_code', $request->code)->first();
        $date = date('Y-m-d');
        $event_date = Carbon::createFromFormat('Y-m-d', $event->event_date);
        $current_date = Carbon::createFromFormat('Y-m-d', $date);
        $diff = $event_date->diffInDays($current_date, false);
        // if($diff < -3 || $diff > 2){
        //         return abort(404);
        // }
    	$guests = EventGuest::where('event_id', $event->id)->where('email', $request->email)->first();
    	if($guests == null){
    		$guest = EventGuest::create([
	    		'event_id' => $event->id,
	    		'name' => $request->name,
	    		'email' => $request->email,
	    	]);
    	}
    	else{
    		$guest = $guests;
    	}
    	Auth::guard('barcode')->login($guest, true);        
		if (Auth::guard('barcode')->check()) {
        	// Authentication passed...
        	return view('upload', compact('guest'));
    	}
    	else{
    		return 'something went wrong';
    	}
    }

    public function storeImage(Request $request){
			
	 	$image = $request->file;
        $filenameWithExt = $image->getClientOriginalName();
        $imageName = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $imageExt = $image->getClientOriginalExtension();
        $originalName = $imageName.'_'.time().'.'.$imageExt;
        $destination_path = public_path('/event_images');
        $image->move($destination_path, $originalName);
        $fileNameToStore = "event_images/".$originalName;
        $setting = EventSetting::where('event_id', $request->event_id)->first();
        if($setting->is_moderation == 0){
            EventImage::create([
            'event_id' => $request->event_id,
            'event_guest_id' => $request->guest_id,
            'image' => $fileNameToStore,
            'moderation' => 0,
            ]);
        }
        else{
            EventImage::create([
            'event_id' => $request->event_id,
            'event_guest_id' => $request->guest_id,
            'image' => $fileNameToStore,
            'moderation' => 1,
            ]);
        }

    }

    public function storeText(Request $request){
    	$event = Event::where('id', $request->event_id)->first();
    	$code = $event->order_code; 
        $guest = EventGuest::where('id', Auth::guard('barcode')->user()->id)->first();
        $setting = EventSetting::where('event_id', $request->event_id)->first();
    	if ($request->text != null){
            if($setting->is_moderation == 0){
        		EventText::create([
        			'text' => $request->text,
        			'event_id' => $request->event_id,
        			'event_guest_id' => $request->guest_id,
        			'color_code' => $request->color,
                    'moderation' => 0
        		]);
            }
            else{
                EventText::create([
                    'text' => $request->text,
                    'event_id' => $request->event_id,
                    'event_guest_id' => $request->guest_id,
                    'color_code' => $request->color,
                    'moderation' => 1
                ]);
            }
    		return view('upload', compact('guest'));
    	}
    	else{
    		return view('upload', compact('guest'));
    	}
    }

    public function sendEmail(Request $request){
        if($request->code){
            $event = Event::where('order_code', $request->code)->first();
            $date = date('Y-m-d');
            if($event == null){
                $message = 'There is no event exist of this ID.';
                return view('info', compact('message'));
            }
            else{
                $event_date = Carbon::createFromFormat('Y-m-d', $event->event_date);
            }
            $current_date = Carbon::createFromFormat('Y-m-d', $date);
            $diff = $event_date->diffInDays($current_date, false);
            if( $diff < 2){
                    return abort(404);
            }
            $images = EventImage::where('event_id', $request->code)->get();
            $arrImages = [];
            foreach($images as $image){
                array_push($arrImages, $image->image);
            }
            $rows = count($arrImages)/6;
            $floor = floor($rows);
            if($floor < $rows){
                $rows = $floor +1;
            }
            $total = count($arrImages);

            return view('image-listing', compact('rows', 'arrImages', 'total'));
        }
        else{
            return abort(404);
        }
    }
}