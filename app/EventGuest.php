<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class EventGuest extends Authenticatable
{
    protected $table = 'event_guests';
    protected $fillable = [

        'name', 'email', 'event_id',

    ];
    public function events(){
    	return $this->hasMany('App\Event');
    }
}
